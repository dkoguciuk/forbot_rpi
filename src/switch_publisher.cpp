#include "ros/ros.h"
#include "forbot_rpi/SwitchByte.h"
#include "wiringPi.h"

int main(int argc, char** argv)
{
	//Inicjacja wiringPi
	if (wiringPiSetup() == -1)
	{
		ROS_INFO("%s","Nie mozna wystartowac wiringPi!\n");
		return 1;
	}

	//Ustawiamy jako wyjscie
	int i;
	for (i=0;i<8;i++) pinMode(i,INPUT);

	//Podciagamy do plusa
	for (i=0;i<8;i++) pullUpDnControl(i,PUD_UP);

	//Inicjacja w systemie ROS
	ros::init(argc, argv, "switch_publisher");

	//Uchwyt do node'a
	ros::NodeHandle n;

	//Publisher
	ros::Publisher switch_publisher = n.advertise<forbot_rpi::SwitchByte>("switch_state",1000);

	//Czestotliwosc publikowania
	ros::Rate loop_rate(1);

	//Wiadomosc do opublikowania
	forbot_rpi::SwitchByte msg;

	//Glowna petla programu
	while (ros::ok())
	{
		//Zerujemy wiadomosc
		msg.switch_byte=0;

		//Petla po wszystkich pinach
		for (i=0;i<8;i++) msg.switch_byte |= (digitalRead(i) << i);

		ROS_INFO("%i\n",msg.switch_byte);
		
		//Publikujemy stan switchy
		switch_publisher.publish(msg);

		//Uspij program wedlug podanej wczesniej czestotliwosci
		loop_rate.sleep();
	}

	//Return
	return 0;
}